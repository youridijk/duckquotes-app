import 'package:duckquotes/pages/register.dart';
import 'package:duckquotes/pages/quotes_page.dart';
import 'package:flutter/material.dart';
import 'package:duckquotes/helpers/variables.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // Verkrijg opgeslagen token en laat login page zien als token null is
  preferences = await SharedPreferences.getInstance();
  String? storedToken = preferences!.getString("token");
  Widget homePage = storedToken == null ? const RegisterPage() : const QuotesPage();
//  Widget homePage = const RegisterPage();
  token = storedToken;

  runApp(App(home: homePage));
}

class App extends StatelessWidget {
  const App({Key? key, required this.home}) : super(key: key);
  final Widget home;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Duck Quotes',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: home,
    );
  }
}
