class LoginException implements Exception {
  final int code;
  final String message;

  LoginException(this.code, this.message) : super();
}