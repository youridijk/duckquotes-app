import 'package:duckquotes/model/vote.dart';

class Quote {
  final int id;
  final String text;
  int voteScore;
  final String sayer;
  final String? creator;
  final DateTime creationDate;
  List<Vote> votes;
  bool? isMyVotePositive;

  Quote(
    this.id,
    this.text,
    this.sayer,
    this.voteScore,
    this.creator,
    this.creationDate,
    this.votes,
    this.isMyVotePositive,
  );

  factory Quote.fromJson(Map<String, dynamic> json) {
    List<Vote> votes = [];
    bool? myVote;
    String? creator;

    if (json.containsKey("votes")) {
      Iterable rawVotes = json["votes"];
      votes = rawVotes.map((vote) => Vote.fromJson(vote)).toList();
    }

    if (json.containsKey("myVotePositive")) {
      myVote = json["myVotePositive"];
    }

    if (json.containsKey("creator")) {
      creator = json["creator"]["username"];
    }

    return Quote(
      json['id'],
      json['quote'],
      json["sayer"]["username"],
      json["vote_score"] ?? json["voteScore"],
      creator,
      DateTime.parse(json["created_at"] ?? json["publicationDate"]),
      votes,
      myVote,
    );
  }
}
