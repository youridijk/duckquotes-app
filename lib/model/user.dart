class User {
  final int id;
  final String username;
  final String? name;

  User(this.id, this.username, this.name);

  User.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        username = json["username"],
        name = json["name"];
}
