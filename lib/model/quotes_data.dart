import 'package:duckquotes/model/quote.dart';
import 'package:duckquotes/model/user.dart';

class QuotesData {
  QuotesData(this.quotes, this.users, this.userData);

  final List<Quote> quotes;
  final List<User> users;
  final User userData;
}