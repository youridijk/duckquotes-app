class Vote {
  final String voter;
  final bool positive;
  final int value;

  Vote(
    this.voter,
    this.positive,
  ) : value = positive ? 1 : -1;

  factory Vote.fromJson(Map<String, dynamic> json) {
    return Vote(
      json.containsKey("user") ? json["user"]["username"] : json["voter"]["username"],
      json.containsKey("isPositive") ? json["isPositive"] : json["is_positive"],
    );
  }
}
