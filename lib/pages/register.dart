import 'dart:convert';

import 'package:duckquotes/helpers/variables.dart';
import 'package:duckquotes/model/login_exception.dart';
import 'package:duckquotes/widgets/big_button.dart';
import 'package:duckquotes/widgets/textfield.dart';
import 'package:duckquotes/pages/quotes_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'login.dart';
import 'package:duckquotes/helpers/methods.dart' as methods;

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  String _error = "";
  bool _showSpinner = false;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordRepeatController =
      TextEditingController();

  final Map<String, String> _userdata = {
    "name": "",
    "username": "",
    "password": "",
    "password_repeat": "",
  };

  void _setError(String error){
    setState(() => _error = error);
  }

  _setShowSpinner(bool showSpinner){
    setState(() => _showSpinner = showSpinner);
  }

  void _register() async {
    if (_userdata.values.contains("")) {
      return _setError("Niet alle velden zijn ingevuld!");
    } else if (_userdata["password"] != _userdata["password_repeat"]) {
      return _setError("Wachtwoorden komen niet overheen");
    }

    _setShowSpinner(true);
    _setError("");

    Map<String, String> body = Map<String, String>.from(_userdata);
    body.remove("password_repeat");

    final response = await http.post(
      Uri.parse("$baseUrl/auth/register"),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Accept': "appliction/json",
      },
      body: jsonEncode(body),
    );

    if (response.statusCode == 201) {
      try {
        await methods.login(context, (_userdata["username"])!, (_userdata["password"])!, const QuotesPage());
      } on LoginException catch(e) {
        _setError(e.message);
      }
    } else {
      setState(() {
        final responseJson = json.decode(response.body);
        print(responseJson);
        if (responseJson.containsKey("error")) {
          _error = (responseJson["error"] ?? responseJson["message"])!;
        } else {
          _error = "Onbekende fout tijdens registreren! "
              "Check je internet verbinding en probeer "
              "later nog eens";
        }
      });
    }
    _setShowSpinner(false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        color: customBlue,
        child: Center(
          child: Column(
            children: [
              _buildTitle(),
              _buildGreeting(),
              Column(
                children: [
                  const Text(
                    "Maak hier je account aan",
                    style: mediumWhite,
                  ),
                  _buildFullNameField(),
                  _buildUsernameField(),
                  _buildPasswordField(),
                  _buildPasswordRepeatField(),
                  _buildErrorWidget(),
                  Visibility(
                    visible: _showSpinner,
                      child: const CircularProgressIndicator(),),
                  _buildRegisterButton(),
                ],
              ),
              const Spacer(),
              Column(
                children: [
                  const Text(
                    "Heb je al een account?",
                    style: mediumWhite,
                  ),
                  _buildLoginButton(context),
                ],
              ),
              const Spacer(flex: 1),
            ],
          ),
        ),
      ),
    );
  }

  Padding _buildGreeting() {
    return const Padding(
      padding: EdgeInsets.only(
        top: 50,
        bottom: 30,
      ),
      child: Text(
        "Welkom bij Duck Quotes",
        style: bigWhite,
      ),
    );
  }

  Padding _buildTitle() {
    return const Padding(
      padding: EdgeInsets.only(top: 70),
      child: Text(
        "Duck Quotes",
        style: TextStyle(
          color: Colors.white,
          fontSize: 30,
          fontFamily: "Lobster",
          fontStyle: FontStyle.italic,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  BigButton _buildLoginButton(BuildContext context) {
    return BigButton(
        text: "Login",
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
          top: 8,
        ),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => const LoginPage()));
        });
  }

  BigButton _buildRegisterButton() {
    return BigButton(
      text: "Registreren",
      padding: const EdgeInsets.only(
        left: 20,
        right: 20,
        top: 8,
      ),
      onPressed: _register,
    );
  }

  Padding _buildErrorWidget() {
    return Padding(
      padding: const EdgeInsets.only(
        top: 8,
        bottom: 20,
        left: 20,
        right: 20,
      ),
      child: Text(
        _error,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.redAccent[700],
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  CustomTextField _buildPasswordRepeatField() {
    return CustomTextField(
      hintText: "Bevestig wachtwoord",
      obscureText: true,
      controller: _passwordRepeatController,
      onChanged: (password) {
        _userdata["password_repeat"] = password;
      },
    );
  }

  CustomTextField _buildPasswordField() {
    return CustomTextField(
      hintText: "Wachtwoord",
      obscureText: true,
      controller: _passwordController,
      onChanged: (password) {
        _userdata["password"] = password;
      },
    );
  }

  CustomTextField _buildUsernameField() {
    return CustomTextField(
      hintText: "Gebruikersnaam",
      obscureText: false,
      controller: _usernameController,
      onChanged: (username) {
        _userdata["username"] = username;
      },
    );
  }

  CustomTextField _buildFullNameField() {
    return CustomTextField(
      hintText: "Volledige naam",
      obscureText: false,
      controller: _nameController,
      onChanged: (name) {
        _userdata["name"] = name;
      },
    );
  }
}
