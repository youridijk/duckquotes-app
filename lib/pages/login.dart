import 'package:duckquotes/helpers/variables.dart';
import 'package:duckquotes/model/login_exception.dart';
import 'package:duckquotes/widgets/big_button.dart';
import 'package:duckquotes/widgets/textfield.dart';
import 'package:duckquotes/pages/quotes_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:duckquotes/helpers/methods.dart' as methods;

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _username = "";
  String _password = "";
  String _error = "";
  bool _showSpinner = false;
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        color: customBlue,
        child: Center(
          child: Column(
            children: [
              _buildTitle(),
              _buildGreeting(),
              const Spacer(),
              Column(
                children: [
                  const Text(
                    "Log hier in op je account",
                    style: mediumWhite,
                  ),
                  _buildUsernameField(),
                  _buildPasswordField(),
                  _buildError(),
                  Visibility(
                    visible: _showSpinner,
                    child: const CircularProgressIndicator(),
                  ),
                  _buildLoginButton(context)
                ],
              ),
              const Spacer(flex: 3)
            ],
          ),
        ),
      ),
    );
  }

  _setError(String error){
    setState(() => _error = error);
  }

  _setShowSpinner(bool showSpinner){
    setState(() => _showSpinner = showSpinner);
  }

  BigButton _buildLoginButton(BuildContext context) {
    return BigButton(
      text: "Login",
      padding: const EdgeInsets.only(
        left: 20,
        right: 20,
        top: 8,
      ),
      onPressed: () async {
        if (_username == "" || _password == "") {
          return _setError("Gebruikersnaam en/of wachtwoord veld is leeg!");
        }
        _setError("");
        _setShowSpinner(true);

        try {
         await methods.login(context, _username, _password, const QuotesPage());
        } on LoginException catch(e) {
          _setError(e.message);
        } finally {
          _setShowSpinner(false);
        }
      },
    );
  }

  Padding _buildError() {
    return Padding(
      padding: const EdgeInsets.only(
        top: 8,
        bottom: 20,
        left: 20,
        right: 20,
      ),
      child: Text(
        _error,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.redAccent[700],
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  CustomTextField _buildPasswordField() {
    return CustomTextField(
      hintText: "Wachtwoord",
      obscureText: true,
      controller: _passwordController,
      onChanged: (password) {
        _password = password;
      },
    );
  }

  CustomTextField _buildUsernameField() {
    return CustomTextField(
      hintText: "Gebruikersnaam",
      obscureText: false,
      controller: _usernameController,
      onChanged: (username) {
        _username = username;
      },
    );
  }

  Padding _buildGreeting() {
    return const Padding(
      padding: EdgeInsets.only(top: 50),
      child: Text(
        "Welkom bij Duck Quotes",
        style: bigWhite,
      ),
    );
  }

  Padding _buildTitle() {
    return const Padding(
      padding: EdgeInsets.only(top: 70),
      child: Text(
        "Duck Quotes",
        style: TextStyle(
          color: Colors.white,
          fontSize: 30,
          fontFamily: "Lobster",
          fontStyle: FontStyle.italic,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
