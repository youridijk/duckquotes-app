import 'package:duckquotes/helpers/futures.dart';
import 'package:duckquotes/widgets/vote_cell.dart';
import 'package:flutter/material.dart';

import '../helpers/methods.dart';
import '../helpers/variables.dart' as globals;
import '../model/quote.dart';
import '../model/vote.dart';

class QuoteDetailPage extends StatefulWidget {
  const QuoteDetailPage({
    Key? key,
    required this.quote,
  }) : super(key: key);

  final Quote quote;

  @override
  _QuoteDetailPageState createState() => _QuoteDetailPageState();
}

class _QuoteDetailPageState extends State<QuoteDetailPage> {
  late Future<Quote> _futureQuote;
  late Quote _quote;

  @override
  void initState() {
    super.initState();
    _futureQuote = fetchQuote(widget.quote.id);
    _quote = widget.quote;
  }

  void _voteOnPressed(BuildContext context, Vote newVote) async {
    await createUpdateVote(_quote.id, newVote.positive)
        .onError((error, stackTrace) {
          print(stackTrace);
      showAlert(
        context,
        "Error voten",
        "Error tijdens voten: $error",
      );
    });
    setState(() {
      _quote.votes
          .removeWhere((vote) => vote.voter == globals.userData?.username);

      if (_quote.isMyVotePositive == newVote.positive) {
        _quote.isMyVotePositive = null;
      } else {
        _quote.isMyVotePositive = newVote.positive;
        _quote.votes.add(newVote);
      }

      if (_quote.votes.isEmpty) {
        _quote.voteScore = 0;
      } else {
        _quote.voteScore = _quote.votes
            .map((vote) => vote.value)
            .reduce((vote1, vote2) => vote1 + vote2);
      }
    });
  }

  Widget _createQuoteView(Quote quote) {
    return Container(
      decoration: globals.roundedBlue,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Wrap(
          spacing: 20,
          runSpacing: 20,
          children: [
            Center(
              child: Text(
                quote.text,
                style: globals.bigWhite,
              ),
            ),
            Center(
              child: Text(
                "- " + quote.sayer,
                style: globals.mediumWhite,
              ),
            ),
            Center(
              child: Text(
                formatDate(quote.creationDate),
                style: globals.smallWhite,
              ),
            ),
            if (quote.creator != null)
              Center(
                child: Text(
                  "Door ${quote.creator!}",
                  style: globals.smallWhite,
                ),
              ),
          ],
        ),
      ),
    );
  }

  Widget _createVotingView(BuildContext context, int quoteId) {
    return Container(
      decoration: globals.roundedBlue,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(
              icon: Icon(
                Icons.thumb_up_alt_outlined,
                color: _quote.isMyVotePositive == true
                    ? Colors.blue
                    : Colors.white,
              ),
              onPressed: () {
                Vote vote = Vote(globals.userData!.username, true);
                _voteOnPressed(context, vote);
              },
            ),
            Text(
              "${_quote.voteScore}",
              style: TextStyle(
                color: getVoteColor(_quote.voteScore),
                fontSize: 20,
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.thumb_down_off_alt_rounded,
                color: _quote.isMyVotePositive == false
                    ? Colors.blue
                    : Colors.white,
              ),
              onPressed: () {
                Vote vote = Vote(globals.userData!.username, false);
                _voteOnPressed(context, vote);
              },
            ),
          ], // thumb_down_off_alt_rounded
        ),
      ),
    );
  }

  Widget _createVotesView(BuildContext context, List<Vote> votes) {
    return Container(
      decoration: globals.roundedBlue,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: [
            const Center(
              child: Text(
                "Votes",
                style: globals.mediumWhite,
              ),
            ),
            ListView.separated(
              shrinkWrap: true,
              physics: const ClampingScrollPhysics(),
              padding: const EdgeInsets.all(8),
              itemCount: votes.length,
              itemBuilder: (BuildContext context, int index) {
                return VoteCell(vote: votes[index]);
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(
                color: Colors.transparent,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).pop(_quote);
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Quote detail'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: FutureBuilder(
            future: _futureQuote,
            builder: (BuildContext context, AsyncSnapshot<Quote> snapshot) {
              if (snapshot.hasData) {
                _quote = snapshot.data!;

                return Wrap(
                  spacing: 20,
                  runSpacing: 20,
                  children: [
                    _createQuoteView(_quote),
                    _createVotingView(
                      context,
                      _quote.id,
                    ),
                    _createVotesView(
                      context,
                      _quote.votes,
                    ),
                  ],
                );
              } else if (snapshot.hasError) {
                if (snapshot.error == "No user found") {
                  logout(context);
                }

                print(snapshot.stackTrace);

                return Wrap(
                  spacing: 20,
                  runSpacing: 20,
                  children: [
                    _createQuoteView(_quote),
                    Center(
                      child: Text("${snapshot.error!}"),
                    ),
                  ],
                );
              }

              return Wrap(
                spacing: 20,
                runSpacing: 20,
                children: [
                  _createQuoteView(_quote),
                  const Center(
                    child: CircularProgressIndicator(),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
