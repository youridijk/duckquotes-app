import 'dart:async';

import 'package:duckquotes/helpers/futures.dart';
import 'package:duckquotes/helpers/methods.dart';
import 'package:duckquotes/model/quotes_data.dart';
import 'package:duckquotes/widgets/add_quote_pop_up.dart';
import 'package:duckquotes/widgets/quote_cell.dart';
import 'package:flutter/material.dart';
import '../helpers/variables.dart' as globals;
import '../model/quote.dart';

class QuotesPage extends StatefulWidget {
  const QuotesPage({Key? key}) : super(key: key);

  @override
  _QuotesPageState createState() => _QuotesPageState();
}

class _QuotesPageState extends State<QuotesPage> {
  late Future<QuotesData> _future;

  @override
  initState() {
    super.initState();
    _future = getQuotesData();
  }

  Widget _quotesListView(AsyncSnapshot<QuotesData> snapshot) {
    if (snapshot.hasData) {
      List<Quote> quotes = snapshot.data!.quotes;
      globals.userData = snapshot.data!.userData;
      globals.users = snapshot.data!.users;

      return Container(
        color: Colors.white,
        child: ListView.separated(
          physics: const BouncingScrollPhysics(
              parent: AlwaysScrollableScrollPhysics(),
          ),
          padding: const EdgeInsets.all(8),
          itemCount: quotes.length,
          itemBuilder: (BuildContext context, int index) {
            return QuoteCell(
              key: UniqueKey(),
              quote: quotes[index],
            );
          },
          separatorBuilder: (BuildContext context, int index) => const Divider(
            color: Colors.transparent,
          ),
        ),
      );
    } else if (snapshot.hasError) {
      //TODO: Mooie error view maken
      if (snapshot.error == "No user found") {
        logout(context);
      }

      print(snapshot.stackTrace);

      return Center(
        child: Text('${snapshot.error}'),
      );
    }

    return const CircularProgressIndicator();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Duck Quotes"),
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            icon: const Icon(
              Icons.logout,
            ),
            onPressed: () => logout(context),
          ),
        ],
      ),
      body: Center(
        child: FutureBuilder<QuotesData>(
          future: _future,
          builder: (context, snapshot) {
            return RefreshIndicator(
              onRefresh: () async {
                setState(() {
                  _future = getQuotesData();
                });
              },
              child: _quotesListView(snapshot),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          showGeneralDialog(
            barrierDismissible: true,
            barrierLabel: "Duck Quotes",
            transitionDuration: const Duration(milliseconds: 400),
            pageBuilder: (context, animation1, animation2) {
              return AddPopUp(users: globals.users);
            },
            context: context,
          ).then((quote) {
            setState(() {
              if (quote != null && quote is Quote) {
                _future.then((quotesData) {
                  QuotesData data = QuotesData(
                    [quote, ...quotesData.quotes],
                    quotesData.users,
                    quotesData.userData,
                  );
                  _future = Future.value(data);
                });
              }
            });
          });
        },
      ),
    );
  }
}
