import 'dart:convert';

import 'package:duckquotes/model/quote.dart';
import 'package:duckquotes/model/user.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:duckquotes/helpers/variables.dart' as globals;

import 'big_button.dart';
import 'package:duckquotes/widgets/textfield.dart';
import 'package:http/http.dart' as http;

class AddPopUp extends StatefulWidget {
  const AddPopUp({Key? key, required this.users}) : super(key: key);

  final List<User> users;

  @override
  _AddPopUpState createState() => _AddPopUpState();
}

class _AddPopUpState extends State<AddPopUp> {
  String _quoteText = "";
  String _sayer = "";
  bool _showSpinner = false;
  String? _error;

  final TextEditingController _controller = TextEditingController();

  Future<Quote> _createQuote(String quoteText, String sayer) async {
    if (sayer == "") {
      throw Exception("Je moet een gebruiker kiezen");
    }

    User user = widget.users.firstWhere((user) => user.username == sayer);

    http.Response response = await http.post(
      Uri.parse("${globals.baseUrl}/quotes"),
      headers: {"Content-Type": "application/json", ...globals.requestHeaders,},
      body: json.encode({
        "quote": quoteText,
        "sayerId": user.id,
        "sayer_id": user.id,
      }),
    );

    final jsonBody = json.decode(response.body);
    print(jsonBody);

    if (response.statusCode == 201) {
      return Quote.fromJson(jsonBody);
    }else{
      throw Exception(jsonBody["error"] ?? jsonBody["message"]);
    }
  }

  _changeSpinnerState(bool enabled) {
    setState(() => _showSpinner = enabled);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          margin: const EdgeInsets.all(8),
          color: globals.customBlue,
          child: Column(
            children: [
              const Padding(
                padding: EdgeInsets.all(12.0),
                child: Text(
                  "Quote aanmaken",
                  style: globals.bigWhite,
                ),
              ),
              CustomTextField(
                hintText: "Quote",
                obscureText: false,
                controller: _controller,
                onChanged: (newValue) => setState(() => _quoteText = newValue),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: DropdownSearch<String>(
                  showSearchBox: true,
                  mode: Mode.MENU,
                  items: widget.users.map((u) => u.username).toList(),
                  onChanged: (newValue) => setState(() => _sayer = newValue!),
                  selectedItem: _sayer,
                  dropdownSearchDecoration: const InputDecoration(
                    border: InputBorder.none,
                  ),
                  popupShape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  dropdownBuilder: (context, value) {
                    return Center(
                      child: Text(value!, style: globals.mediumWhite),
                    );
                  },
                ),
              ),
              BigButton(
                text: "Maak quote",
                height: 40,
                fontSize: 20,
                padding: const EdgeInsets.all(20),
                onPressed: _quoteText == "" || _sayer == ""
                    ? null
                    : () {
                        _changeSpinnerState(true);
                        _createQuote(_quoteText, _sayer).then((Quote quote) {
                          _changeSpinnerState(false);
                          Navigator.of(context).pop(quote);
                        }).catchError((error) {
                          _changeSpinnerState(false);
                          setState(() => _error = error.toString());
                        });
                      },
                // onPressed: null,
              ),
              Visibility(
                visible: _showSpinner,
                child: const Padding(
                  padding: EdgeInsets.only(bottom: 20),
                  child: CircularProgressIndicator(),
                ),
              ),
              Visibility(
                visible: _error != null,
                child: Text("$_error"),
              )
            ],
          ),
        ),
      ],
    );
  }
}
