import 'package:duckquotes/helpers/methods.dart';
import 'package:duckquotes/helpers/variables.dart';
import 'package:duckquotes/model/quote.dart';
import 'package:duckquotes/pages/quote_detail_page.dart';
import 'package:flutter/material.dart';

class QuoteCell extends StatefulWidget {
  const QuoteCell({required Key key, required this.quote}) : super(key: key);

  final Quote quote;

  @override
  _QuoteCellState createState() => _QuoteCellState();
}

class _QuoteCellState extends State<QuoteCell> {
  late Quote _quote;

  @override
  void initState() {
    super.initState();
    _quote = widget.quote;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        decoration: roundedBlue,
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  _quote.text,
                  style: bigWhite,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "- " + _quote.sayer,
                      style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontFamily: "Helvetica",
                        fontSize: 18,
                      ),
                    ),
                    Text(
                      _quote.voteScore.toString(),
                      style: TextStyle(
                        color: getVoteColor(_quote.voteScore),
                        fontWeight: FontWeight.bold,
                        fontFamily: "Helvetica",
                        fontSize: 24,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute<Quote>(
            builder: (context) {
              return QuoteDetailPage(
                quote: _quote,
              );
            },
          ),
        ).then((returnedQuote) {
          if(returnedQuote != null){
            setState(() => _quote = returnedQuote);
          }
        });
      },
    );
  }
}

