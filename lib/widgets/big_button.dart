import 'package:flutter/material.dart';

class BigButton extends StatelessWidget {
  const BigButton({
    Key? key,
    required this.text,
    this.height = 70,
    this.fontSize = 30,
    required this.padding,
    required this.onPressed,
  }) : super(key: key);

  final String text;
  final EdgeInsetsGeometry padding;
  final Function()? onPressed;
  final double height;
  final double fontSize;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: SizedBox(
        height: height,
        width: double.infinity,
        child: TextButton(

          child: Text(
            text,
            style: TextStyle(
              color: onPressed == null ? Colors.grey : Colors.black,
              fontSize: fontSize,
              fontWeight: FontWeight.bold,
            ),
          ),
          style: ButtonStyle(
            backgroundColor: MaterialStateColor.resolveWith(
              (states) => Colors.white,
            ),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
            ),
          ),
          onPressed: onPressed,
        ),
      ),
    );
  }
}
