import 'package:duckquotes/model/vote.dart';
import 'package:flutter/material.dart';
import 'package:duckquotes/helpers/variables.dart' as globals;

class VoteCell extends StatelessWidget {
  const VoteCell({Key? key, required this.vote}) : super(key: key);

  final Vote vote;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: globals.roundedWhite,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              vote.voter,
              style: const TextStyle(
                fontSize: 16,
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              vote.positive ? "👍" : "👎",
              style: const TextStyle(
                fontSize: 30,
              ),
            )
          ],
        ),
      ),
    );
  }
}
