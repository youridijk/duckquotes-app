import 'package:duckquotes/model/user.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

String? token = "";
SharedPreferences? preferences;
List<User> users = [];
User? userData;

// const baseUrl = "http://localhost:8080/api/v1";
const baseUrl = "https://duckquotes-backend.dijk.tk/api/v1";

const customBlue = Color.fromARGB(255, 56, 122, 197);

const roundedBlue = BoxDecoration(
  borderRadius: BorderRadius.all(Radius.circular(10)),
  color: customBlue,
);

const roundedWhite = BoxDecoration(
  borderRadius: BorderRadius.all(Radius.circular(10)),
  color: Colors.white,
);

const bigWhite = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontFamily: "Helvetica",
  fontSize: 22,
);

const mediumWhite = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontFamily: "Helvetica",
  fontSize: 20,
);

const smallWhite = TextStyle(
  color: Colors.white,
  fontFamily: "Helvetica",
  fontSize: 18,
);

Map<String, String> requestHeaders = {
  "Authorization": "Bearer $token",
  "Accept": "application/json"
};
