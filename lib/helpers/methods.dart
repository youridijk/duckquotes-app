import 'package:duckquotes/model/login_exception.dart';
import 'package:duckquotes/pages/register.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'variables.dart' as globals;
import 'futures.dart' as futures;

Color? getVoteColor(int voteScore) {
  if (voteScore == 0) {
    return Colors.grey;
  } else if (voteScore < 0) {
    return Colors.redAccent[700];
  } else {
    return Colors.greenAccent[700];
  }
}

String formatDate(DateTime dateTime) {
  DateFormat dateFormat = DateFormat("H:m, d MMM y");
  return dateFormat.format(dateTime);
}

void showAlert(BuildContext context, String title, String content){
  showDialog(context: context, builder: (BuildContext buildContext){
      return AlertDialog(
        title: Text(title),
        content: Text(content),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text("OK"),
          )
        ],
      );
  });
}

login(BuildContext context, String username, String password, Widget nextPage) async {
  try {
    String token = await futures.login(username, password);
    globals.token = token;
    globals.preferences!.setString("token", token);

    Navigator.of(context).pushReplacement(
      MaterialPageRoute<void>(builder: (context) {
        return nextPage;
      }),
    );
  } on LoginException catch(e) {
    if (e.code == 401) {
      throw LoginException(e.code, "Gebruikersnaam en/of wachtwoord incorrect");
    } else {
      throw LoginException(e.code, e.message);
    }
  }
}

logout(BuildContext context){
  globals.preferences!.remove("token");
  Navigator.of(context).push(
    MaterialPageRoute(
      builder: (BuildContext context) {
        return const RegisterPage();
      },
    ),
  );
}