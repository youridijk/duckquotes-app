import 'dart:convert';

import 'package:duckquotes/model/login_exception.dart';
import 'package:duckquotes/model/quote.dart';
import 'package:duckquotes/model/quotes_data.dart';
import 'package:duckquotes/model/user.dart';
import 'package:duckquotes/helpers/variables.dart' as globals;
import 'package:http/http.dart' as http;

Future<String> login(String username, String password) async {
  final response = await http.post(
    Uri.parse("${globals.baseUrl}/authenticate"),
    //    Uri.parse("${globals.baseUrl}/auth/login"),
    headers: <String, String>{
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: jsonEncode(<String, String>{
      "username": username,
      "password": password,
    }),
  );

  final jsonBody = json.decode(response.body);

  if (response.statusCode == 201) {
    return jsonBody["token"];
  } else {
    print(jsonBody);
    throw LoginException(
      response.statusCode,
      jsonBody["error"] ?? jsonBody["message"],
    );
  }
}

Future<List<Quote>> fetchQuotes() async {
  final response = await http.get(
    Uri.parse('${globals.baseUrl}/quotes'),
    headers: globals.requestHeaders,
  );
  final jsonBody = json.decode(response.body);

  if (response.statusCode == 200) {
    return jsonBody.map<Quote>((q) => Quote.fromJson(q)).toList();
  } else {
    throw Exception(jsonBody["error"] ?? jsonBody["message"]);
  }
}

Future<List<User>> getUsers() async {
  final response = await http.get(
    Uri.parse("${globals.baseUrl}/users/usernames"),
    headers: globals.requestHeaders,
  );

  final jsonBody = json.decode(response.body);

  if (response.statusCode == 200) {
    return jsonBody.map<User>((user) => User.fromJson(user)).toList();
  } else {
    throw Exception(jsonBody["error"]);
  }
}

Future<User> getUserData() async {
  final response = await http.get(
    Uri.parse("${globals.baseUrl}/users"),
    headers: globals.requestHeaders,
  );

  final jsonBody = json.decode(response.body);

  if (response.statusCode == 200) {
    return User.fromJson(jsonBody);
  } else {
    throw Exception(jsonBody["error"] ?? jsonBody["message"]);
  }
}

Future<QuotesData> getQuotesData() async {
  List result = await Future.wait([
    fetchQuotes(),
    getUsers(),
    getUserData(),
  ]);

  return QuotesData(result[0], result[1], result[2]);
}

Future<Quote> fetchQuote(int id) async {
  http.Response response = await http.get(
    Uri.parse("${globals.baseUrl}/quotes/$id"),
    headers: globals.requestHeaders,
  );

  final jsonBody = json.decode(response.body);

  if (response.statusCode == 200) {
    return Quote.fromJson(jsonBody);
  } else {
    throw Exception(jsonBody["error"] ?? jsonBody["message"]);
  }
}

Future<void> createUpdateVote(int quoteId, bool? isPositive) async {
  http.Response response;
  // int expectedStatusCode = isPositive == null ? 204 : 201;
  int expectedStatusCode = 201;

  if (isPositive == null) {
    response = await http.delete(
      Uri.parse("${globals.baseUrl}/quotes/$quoteId/votes/"),
      headers: globals.requestHeaders,
    );
  } else {
    final headers = {
      ...globals.requestHeaders,
      'Content-Type': 'application/json',
    };

    response = await http.post(
      Uri.parse("${globals.baseUrl}/quotes/$quoteId/votes/"),
      headers: headers,
      body: jsonEncode(<String, bool>{
        "isPositive": isPositive,
        "is_positive": isPositive,
      }),
    );
  }

  if (response.statusCode != expectedStatusCode) {
    final jsonBody = json.decode(response.body);
    throw Exception(jsonBody["error"] ?? jsonBody["message"]);
  }
}
