# Duck Quotes
A simple app made for friends to write down quotes with the ability to vote for on quote.

## Thanks to
[Noha van der Griendt](https://gitlab.com/NohavdGriendt) for making and designing the [website](https://duckquotes.herokuapp.com/)

[Ruben Dijkhof](https://github.com/RubenDijkhof) for designing the app

